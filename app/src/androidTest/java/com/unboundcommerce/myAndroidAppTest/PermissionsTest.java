package com.unboundcommerce.myAndroidAppTest;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication.MainActivity;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by anthonyameertoorie on 6/4/15.
 */
public class PermissionsTest {

    @BeforeClass
    public void setUp() {
        // code that will be invoked when this test is instantiated
    }

    @AfterClass
    public void tearDown(){
        // code that will be invoked when this test is destroyed
    }

    @Test(groups = { "unit" })
    public void testPermissionsForApplication() throws XmlPullParserException, ParserConfigurationException, IOException, SAXException, TransformerException {
        File currentTestFile = new File("PermissionsTest.java");
        assertEquals(currentTestFile.isDirectory(), false);
        File baseDir = currentTestFile.getAbsoluteFile().getParentFile();
        File xmlFile = new File(baseDir.getAbsolutePath().toString() + "/app/src/main/AndroidManifest.xml");
        assertNotEquals(xmlFile, null);

        String xmlAsString = convertXMLFileToString(xmlFile.getAbsolutePath());
        int manifestIndexStart = xmlAsString.indexOf("<manifest");
        int applicationIndexStart = xmlAsString.indexOf("<application");
        String assessXMLPermissions = xmlAsString.substring(manifestIndexStart, applicationIndexStart);
        int manifestCloseTag = assessXMLPermissions.indexOf(">");
        assessXMLPermissions = assessXMLPermissions.substring(manifestCloseTag);

        assertTrue(assessXMLPermissions.contains("<uses-permission android:name=\"android.permission.CAMERA\"/>"));
        assertTrue(assessXMLPermissions.contains("<uses-permission android:name=\"android.permission.INTERNET\"/>"));
    }

    public String convertXMLFileToString(String fileName) throws TransformerException, ParserConfigurationException, IOException, SAXException {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            InputStream inputStream = new FileInputStream(new File(fileName));
            org.w3c.dom.Document doc = documentBuilderFactory.newDocumentBuilder().parse(inputStream);
            StringWriter stw = new StringWriter();
            Transformer serializer = TransformerFactory.newInstance().newTransformer();
            serializer.transform(new DOMSource(doc), new StreamResult(stw));
            return stw.toString();

    }
}
