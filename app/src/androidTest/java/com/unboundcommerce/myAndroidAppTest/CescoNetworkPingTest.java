package com.unboundcommerce.myAndroidAppTest;

import android.util.Log;

import com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication.WebActivity;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by anthonyameertoorie on 6/3/15.
 */
public class CescoNetworkPingTest {
    private static final String URL_BASE_CESCO = "stage.unboundcommerce.com";


    @BeforeClass
    public void setUp() {
        // code that will be invoked when this test is instantiated
    }

    @AfterClass
    public void tearDown(){
        // code that will be invoked when this test is destroyed
    }

    @Test(groups = { "integration" })
    public void pingCescoTest() {
        assertTrue(isReachable(URL_BASE_CESCO));
    }


    private boolean isReachable(String url){
        Socket socket = null;
        boolean reachable = false;
        try {
            socket = new Socket(url, 80);
            reachable = true;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (socket != null) try { socket.close(); } catch(IOException e) {}
        }
        return reachable;
    }

}
