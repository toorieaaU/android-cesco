package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SearchView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Created by anthonyameertoorie on 6/2/15.
 */
public class WebActivity extends ActionBarActivity {
    public static final String URL_HOME = "http://stage.unboundcommerce.com/gstore/cesco/?fromClient=true";
    public static final String URL_CONTACT = "http://stage.unboundcommerce.com/gstore/cesco/contents/aboutUs/contactUs.jsp?fromClient=true";
    public static final String CART_PAGE_FULL_SITE = "http://stage.unboundcommerce.com/mproxy/controller?storeId=cesco&op=viewCart";
    public static final String URL_CART = CART_PAGE_FULL_SITE + "&fromClient=true";
    public static final String URL_LOGIN = "http://stage.unboundcommerce.com/mproxy/controller?storeId=cesco&op=appLogin&userLogin=&userPassword=&forgotPasswordEmail=&LoginFormController=Sign%20In&submitType=login";
    public static final String URL_LOGIN_STATIC = "http://stage.unboundcommerce.com/mproxy/controller?storeId=cesco&op=myaccount&fromClient=true" +
            "";
    public static final String URL_REFRESH_LOGIN_STATUS = "http://stage.unboundcommerce.com/mproxy/controller?storeId=cesco&op=checkLoginStatus";
    /* CONSTANTS */
    public static final String URL_LOGGED_IN_APP_COUNTER = "http://stage.unboundcommerce.com/mproxy/controller?storeId=cesco&op=appItemCounter";
    private static final String APPEND_URL_CLIENT = "/?fromClient=true";
    private static final int offsetWebViewOnVerticleAxis = Graphics.getPixelsFromPercentFromY(8);
    /* Timer configurations*/
    private static final int SLEEP_TIMER_FOR_SLIDE_OUT_ANIMATION = 500;
    private static final int SLEEP_TIMER_FOR_REFRESHING_LOGGED_IN_USER_CART_IN_SECONDS = 1;
    public static final int REFRESH_INTERVAL_FOR_REFRESH_IN_MINUTES = 1;
    /* Urls  */
    public static final String URL_MENU = "http://ux.ivorscott.com/cesco/menuOut.html";
    public static Semaphore lockForGettingCartDataForUserNotLoggedIn = new Semaphore(1);
    //this message is handled by two Web Clients
    public static volatile ProgressDialog mProgressDialog;
    private static volatile OrientationEventListener myOrientationEventListener;
    /* UI elements*/
    private static ImageView menuIcon;
    private static ImageView scanButton;
    private static SearchView mySearchView;
    private static Button numberOfItemsInCartDisplayToUserAsCircle;
    /* Web Views*/
    private static volatile WebView slideOutMenuWebView;    //navbar interface
    private static volatile WebView myWebView; //default webview pane
    private static volatile WebView myResourceWebViewForNotLoggedInUsers; //default webview pane
    //Settings
    private static volatile boolean slideOutMenuIsOut = false;
    private static RelativeLayout myDefaultLayout;

    //private static ImageView myBackgroundForToolbar = null;
    //toolbar elements
    private static Button notificationBox = null;
    private static Button signInButton = null;
    private static Button shopButton = null;
    private static Button contactButton = null;
    private static Button cartButton = null;

    private static boolean initOnceForBackgroundToolbar = true;
    private static RelativeLayout backgroundForToolbar;

    private static boolean doOnceOnLoad = true;

    int sdk = android.os.Build.VERSION.SDK_INT;

    @Override
    public void onResume() {
        super.onResume();
        myOrientationEventListener.enable();
    }

    @Override
    public void onPause() {
        super.onPause();
        myOrientationEventListener.disable();
    }

    //javascript checks
    private static volatile boolean pageDoesContainCheckBoxes = false;

    public static void webViewWillRetainWithinApp(WebView myWebView, WebViewClient myClient) {
        myWebView.setWebViewClient(myClient);
    }

    private static String readUrl(String urlString) throws Exception {
        URL myUrl = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection();
        connection.setRequestMethod("GET");
        if (WebViewClientCustomized.getCookies() != null) {
            connection.setRequestProperty("Cookie", WebViewClientCustomized.getCookies());
            Log.d("Service", "Cookie set");
            Log.d("Service", WebViewClientCustomized.getCookies());
            connection.setRequestProperty("Connection", "Keep-Alive");
        }
        connection.connect();


        int code = connection.getResponseCode();

        Log.d("Service", "Status is " + code);
        Log.d("Service", "Response is " + connection.getResponseMessage());


        switch (code) {
            case 200:
            case 201:
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                Log.d("Service", "Caught the json" + sb);
                return sb.toString();
        }

        connection.disconnect();
        return connection.getResponseMessage();
    }

    public static void deleteAllCookies(Activity context) {
        //delete all cookies
        CookieSyncManager.createInstance(context);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
    }

    protected void onCreate(Bundle savedInstanceState) {
        {
            Display display = getWindowManager().getDefaultDisplay();
            Point displaySize = new Point();
            display.getSize(displaySize);
            Graphics.setDisplaySize(displaySize);
            Log.d("Passing 'Point' Object", Integer.toString(displaySize.x));
            Log.d("Passing 'Point' Object", Integer.toString(displaySize.y));
        }

        super.onCreate(savedInstanceState);


        final ActionBarActivity dynamicStateWebActivity = this;
        myOrientationEventListener
                = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {

            /**
             * Called when the orientation of the device has changed.
             * orientation parameter is in degrees, ranging from 0 to 359.
             * orientation is 0 degrees when the device is oriented in its natural position,
             * 90 degrees when its left side is at the top, 180 degrees when it is upside down,
             * and 270 degrees when its right side is to the top.
             * {@link #ORIENTATION_UNKNOWN} is returned when the device is close to flat
             * and the orientation cannot be determined.
             *
             * @param orientation The new orientation of the device.
             * @see #ORIENTATION_UNKNOWN
             */
            @Override
            public void onOrientationChanged(int orientation) {
                if (pageDoesContainCheckBoxes == false) {
                    return;
                }

                final AbsoluteLayout parentLayout = new AbsoluteLayout(dynamicStateWebActivity);
                final AbsoluteLayout.LayoutParams params = new AbsoluteLayout.LayoutParams(Graphics.displaySize.y, Graphics.displaySize.x, 0, 0);
                parentLayout.setBackgroundColor(Color.BLACK);

                final Integer orientationObj = orientation;
                final int PORTRAIT_MODE = 1;
                final int RIGHT_ROTATION = 2;
                final int LEFT_ROTATION = 3;
                int rotationStraightInDegrees = 0;
                int rotationRightInDegrees = 90;
                int rotationLeftInDegrees = 270;
                int error = 10;
                if (orientationObj == -1) {
                    return;
                }
                Log.d("Orientation", orientationObj.toString());

                if ((orientationObj > (rotationRightInDegrees - error)) && (orientationObj < (rotationRightInDegrees + error))) {
                    Log.d("Orientation", "Right,Lanscape");

                    if (Graphics.GLOBAL_ORIENTATION == RIGHT_ROTATION) { //right
                        return;
                    }

                    final String backUrl = myWebView.getOriginalUrl();

                    final String compareValuesWithJSFunctionCall = "goCompare();";
                    Graphics.GLOBAL_ORIENTATION = RIGHT_ROTATION; //right

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        /**
                         * Starts executing the active part of the class' code. This method is
                         * called when a thread is started that has been created with a class which
                         * implements {@code Runnable}.
                         */
                        @Override
                        public void run() {
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                        }
                    });

                    handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {


                        /**
                         * Starts executing the active part of the class' code. This method is
                         * called when a thread is started that has been created with a class which
                         * implements {@code Runnable}.
                         */
                        @Override
                        public void run() {
                            //TODO: More specific SDK Testing
                            if (sdk >= Build.VERSION_CODES.KITKAT) {

                            }else{
                                return;
                            }
                            myWebView.loadUrl(backUrl);
                        }
                    },0);

                } else if ((orientationObj > (rotationLeftInDegrees - error)) && (orientationObj < (rotationLeftInDegrees + error))) {
                    Log.d("Orientation", "Left,Landscape");

                    if (Graphics.GLOBAL_ORIENTATION == LEFT_ROTATION) { //left
                        return;
                    }

                    final String backUrl = myWebView.getOriginalUrl();
                    final String compareValuesWithJSFunctionCall = "goCompare();";
                    Graphics.GLOBAL_ORIENTATION = LEFT_ROTATION; //left
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        /**
                         * Starts executing the active part of the class' code. This method is
                         * called when a thread is started that has been created with a class which
                         * implements {@code Runnable}.
                         */
                        @Override
                        public void run() {
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                        }
                    });
                    handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {


                        /**
                         * Starts executing the active part of the class' code. This method is
                         * called when a thread is started that has been created with a class which
                         * implements {@code Runnable}.
                         */
                        @Override
                        public void run() {
                            //TODO: More specific SDK Testing
                            if (sdk >= Build.VERSION_CODES.KITKAT) {

                            }else{
                                return;
                            }
                            myWebView.loadUrl(backUrl);
                        }
                    }, 0);
                } else if ((orientationObj > (rotationStraightInDegrees - error)) && (orientationObj < (rotationStraightInDegrees + error))) {
                    Log.d("Orientation", "Straight,Normal,Portrait");

                    if (Graphics.GLOBAL_ORIENTATION == PORTRAIT_MODE) {
                        return;
                    }

                    Graphics.GLOBAL_ORIENTATION = PORTRAIT_MODE;

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        /**
                         * Starts executing the active part of the class' code. This method is
                         * called when a thread is started that has been created with a class which
                         * implements {@code Runnable}.
                         */
                        @Override
                        public void run() {
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                            if (WebViewClientCustomized.allUrlsThatHaveBeenLoadedInList.size() == 0) {
                                Log.d("Orientation", "Redirecting url home");
                                myWebView.loadUrl(URL_HOME);
                                return;
                            }
                            for (int i = WebViewClientCustomized.allUrlsThatHaveBeenLoadedInList.size() - 1; i >= 0; i--) {
                                if (WebViewClientCustomized.allUrlsThatHaveBeenLoadedInList.get(i).contains("category") &&
                                        !WebViewClientCustomized.allUrlsThatHaveBeenLoadedInList.get(i).contains("compareCescoNumbers")) {
                                    myWebView.loadUrl(WebViewClientCustomized.allUrlsThatHaveBeenLoadedInList.get(i));
                                    Log.d("Orientation", "Redirecting url to base");
                                    Log.d("Javascript", "(related abstractly)Redirecting url to base");
                                    return;
                                }
                            }
                        }
                    });
                }
            }
        };

        backgroundForToolbar = new RelativeLayout(this);
        backgroundForToolbar.setLayoutParams(new RelativeLayout.LayoutParams(Graphics.getDisplaySize().x, offsetWebViewOnVerticleAxis));

        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) backgroundForToolbar.getLayoutParams();
        myParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        int sdk = android.os.Build.VERSION.SDK_INT;

        BitmapDrawable myBackground = (BitmapDrawable) ToolBarItems.getDrawable("tile", this);
        myBackground.setTileModeX(Shader.TileMode.REPEAT);

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            backgroundForToolbar.setBackgroundDrawable(myBackground);
        } else {
            backgroundForToolbar.setBackground(myBackground);
        }


        Graphics.lockOrientation(this);

        if (myOrientationEventListener.canDetectOrientation()) {
            myOrientationEventListener.enable();
        }


        if (WebActivity.lockForGettingCartDataForUserNotLoggedIn.availablePermits() == 0) {
            WebActivity.lockForGettingCartDataForUserNotLoggedIn.release();
        }


        //construct toolbar
        try {
            ToolBarItems.constructToolbarItems(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //toolbar elements
        notificationBox = ToolBarItems.getNotificationBox();
        signInButton = ToolBarItems.getSignInButton();
        shopButton = ToolBarItems.getShopButton();
        contactButton = ToolBarItems.getContactButton();
        cartButton = ToolBarItems.getCartButton();

        mProgressDialog = new ProgressDialog(this);
        myDefaultLayout = new RelativeLayout(this);

        numberOfItemsInCartDisplayToUserAsCircle = new Button(this);
        numberOfItemsInCartDisplayToUserAsCircle.setGravity(Gravity.CENTER);
        if (Graphics.getPixelsFromPercentFromX(100) > 800) {
            numberOfItemsInCartDisplayToUserAsCircle.setTextSize(8);
        } else {
            numberOfItemsInCartDisplayToUserAsCircle.setTextSize(10);
        }
        numberOfItemsInCartDisplayToUserAsCircle.setText("0");
        double scaleFactor = Graphics.getPixelsFromPercentFromX(100) / 1080.0;
        Log.d("scaleValue", String.valueOf(scaleFactor));
        Graphics.setButtonToOrangeCircleWithColorAndSize(numberOfItemsInCartDisplayToUserAsCircle, (int) (100 * scaleFactor), "#FFA500");
        myDefaultLayout.post(new Runnable() {
            /**
             * Starts executing the active part of the class' code. This method is
             * called when a thread is started that has been created with a class which
             * implements {@code Runnable}.
             */
            @Override
            public void run() {
                RelativeLayout.LayoutParams myLayoutForNumberOfItemsInCartTemp = (RelativeLayout.LayoutParams) numberOfItemsInCartDisplayToUserAsCircle.getLayoutParams();
                myLayoutForNumberOfItemsInCartTemp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                myLayoutForNumberOfItemsInCartTemp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                double scaleFactorInternal = Graphics.getPixelsFromPercentFromY(100) / 800.0;
                Log.d("scale", String.valueOf(scaleFactorInternal));
                int shiftPixels = (scaleFactorInternal > 1.0) ? 7 : 0;
                Log.d("scale", String.valueOf(shiftPixels));
                int offsetForOrangeNumberInCartDisplayForSmallScreens = (Graphics.getPixelsFromPercentFromX(100) <= 540) ? 6 : 0;

                myLayoutForNumberOfItemsInCartTemp.setMargins(0, 0, Graphics.getPixelsFromPercentFromX(5) + numberOfItemsInCartDisplayToUserAsCircle.getWidth() / 6 - 3 + (offsetForOrangeNumberInCartDisplayForSmallScreens), Graphics.getPixelsFromPercentFromY(ToolBarItems.DEFAULT_PERCENT_MARGIN_BOTTOM_AS_PERCENT) + cartButton.getHeight() / 2 - (int) (scaleFactorInternal * shiftPixels));
                numberOfItemsInCartDisplayToUserAsCircle.setLayoutParams(myLayoutForNumberOfItemsInCartTemp);
            }
        });


        scanButton = new ImageView(this);
        mySearchView = new SearchView(this);
        Graphics.setFontColorOfSearchView(mySearchView, Color.BLACK);
        mySearchView.setBackgroundColor(Color.WHITE);
        RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        mySearchView.setLayoutParams(layoutparams);
        mySearchView.getLayoutParams().width = Graphics.getPixelsFromPercentFromX(83);
        mySearchView.getLayoutParams().height = Graphics.getPixelsFromPercentFromY(6);
        RelativeLayout.LayoutParams myLayoutParamsForSV = (RelativeLayout.LayoutParams) mySearchView.getLayoutParams();
        myLayoutParamsForSV.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        myLayoutParamsForSV.setMargins(0, 2, Graphics.getPixelsFromPercentFromX(4), 0);
        mySearchView.setLayoutParams(myLayoutParamsForSV);
        mySearchView.setY(Graphics.getPixelsFromPercentFromY(1));
        mySearchView.setFocusable(true);
        mySearchView.setIconifiedByDefault(false);
        mySearchView.clearFocus();
        final Intent barCodeScannerActivity = new Intent(this, BarCodeScan.class);
        mySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String tmpQueryParser = query.replaceAll(" ", "%20");
                BarCodeScan.setSearchStringQuery(tmpQueryParser);
                startActivity(barCodeScannerActivity);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mySearchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if (!queryTextFocused) {
                    Log.d("FOCUS", "true");
                    hideKeyboard(view);
                }
            }
        });


        try {
            menuIcon = Graphics.getImageView("Menu.png", this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert menuIcon != null;
        Log.d("Image threshhold 2", String.valueOf(ToolBarItems.IMAGE_SCALE_THRESHOLD_FOR_IMAGEVIEWS));
        final int ADDITIONAL_MINSIZE_FOR_MENUICON = 10;
        Graphics.setImageLocationAndSize(menuIcon, Graphics.getPixelsFromPercentFromX(2), Graphics.getPixelsFromPercentFromY(2), (int) (48 * ToolBarItems.IMAGE_SCALE_THRESHOLD_FOR_IMAGEVIEWS + ADDITIONAL_MINSIZE_FOR_MENUICON), (int) (32 * ToolBarItems.IMAGE_SCALE_THRESHOLD_FOR_IMAGEVIEWS) + ADDITIONAL_MINSIZE_FOR_MENUICON);

        //main webview for site
        myWebView = new WebView(this);
        webViewWillRetainWithinApp(myWebView, new WebViewClientCustomized(mProgressDialog, URL_HOME, APPEND_URL_CLIENT, offsetWebViewOnVerticleAxis, this));
        myWebView.setWebChromeClient(new CustomWebChromeClient(mProgressDialog, 20));
        Graphics.setWebViewDimensionsWithDimensions(myWebView, Graphics.getDisplaySize().x, Graphics.getDisplaySize().y - offsetWebViewOnVerticleAxis * 2);
        setJavaScriptEnabledForWebView(myWebView, true);
        Graphics.setDefaultWebViewToLoadAllAssetsOnScreen(myWebView, true);
        //setCenterInParentRuleToMyWebView(myWebView);
        setWebViewCache(myWebView, this);
        WebViewClientCustomized.setMainWebViewReference(myWebView);
        myWebView.addJavascriptInterface(new
                MyCheckboxValidator(), "checkboxValidator");

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            /**
             * Starts executing the active part of the class' code. This method is
             * called when a thread is started that has been created with a class which
             * implements {@code Runnable}.
             */
            @Override
            public void run() {
                if (Graphics.GLOBAL_ORIENTATION == 1) {
                    if(doOnceOnLoad == true) {
                        doOnceOnLoad = false;
                        myWebView.loadUrl(URL_HOME);
                    }
                }
            }

        });


        //resource model for non logged in users
        myResourceWebViewForNotLoggedInUsers = new WebView(this);
        webViewWillRetainWithinApp(myResourceWebViewForNotLoggedInUsers, new WebViewClientCustomized(null, URL_HOME, APPEND_URL_CLIENT, offsetWebViewOnVerticleAxis, this));
        myResourceWebViewForNotLoggedInUsers.setWebChromeClient(new CustomWebChromeClient(null, -1));
        Graphics.setWebViewDimensionsWithDimensions(myResourceWebViewForNotLoggedInUsers, Graphics.getDisplaySize().x, Graphics.getDisplaySize().y - offsetWebViewOnVerticleAxis * 3);

        setJavaScriptEnabledForWebView(myResourceWebViewForNotLoggedInUsers, true);
        Graphics.setDefaultWebViewToLoadAllAssetsOnScreen(myResourceWebViewForNotLoggedInUsers, true);
        setCenterInParentRuleToMyWebView(myResourceWebViewForNotLoggedInUsers);
        setWebViewCache(myResourceWebViewForNotLoggedInUsers, this);
        WebViewClientCustomized.setResourceForNotLoggedInUser(myResourceWebViewForNotLoggedInUsers);
        myResourceWebViewForNotLoggedInUsers.addJavascriptInterface(new
                MyJavascriptCartInterface(), "HtmlViewer");
        WebViewClientCustomized.setResourceForNotLoggedInUser(myResourceWebViewForNotLoggedInUsers);
        // myResourceWebViewForNotLoggedInUsers.loadUrl(CART_PAGE_FULL_SITE);


        //webview navbar
        slideOutMenuWebView = new WebView(this);
        webViewWillRetainWithinApp(slideOutMenuWebView, new WebViewClientCustomized(null, URL_MENU, "", offsetWebViewOnVerticleAxis, this));
        slideOutMenuWebView.setWebChromeClient(new CustomWebChromeClient(null, -1));
        Graphics.setWebViewDimensionsWithDimensions(slideOutMenuWebView, (int) (Graphics.getDisplaySize().x * .8), Graphics.getDisplaySize().y - offsetWebViewOnVerticleAxis * 2);

        setJavaScriptEnabledForWebView(slideOutMenuWebView, true);
        Graphics.setDefaultWebViewToLoadAllAssetsOnScreen(slideOutMenuWebView, true);
        setCenterInParentRuleToMyWebView(slideOutMenuWebView);
        setWebViewCache(slideOutMenuWebView, this);
        WebViewClientCustomized.setNavBarReference(slideOutMenuWebView);
        if (Graphics.GLOBAL_ORIENTATION == 1) {
            slideOutMenuWebView.loadUrl(URL_MENU);
        }
        alignToLeftAndBelowOffsetOutsideScreen(slideOutMenuWebView, offsetWebViewOnVerticleAxis);
        slideOutMenuWebView.setX(0);
        slideOutMenuWebView.getSettings().setSupportZoom(false);
        slideOutMenuWebView.getSettings().setBuiltInZoomControls(false);
        slideOutMenuWebView.getSettings().setUseWideViewPort(false);
        slideOutMenuWebView.setHorizontalScrollBarEnabled(false);

        slideOutMenuWebView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                float m_downX = 0;
                if (event.getPointerCount() > 1) {
                    //Multi touch detected
                    return true;
                }

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        // save the x
                        m_downX = event.getX();
                    }
                    break;

                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP: {
                        // set x so that it doesn't move
                        event.setLocation(m_downX, event.getY());
                    }
                    break;

                }

                return false;
            }
        });

        //toolbar actions
        shopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.loadUrl(URL_HOME);
            }
        });

        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.loadUrl(URL_CONTACT);
            }
        });

        cartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.loadUrl(URL_CART);
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.loadUrl(URL_LOGIN_STATIC);
            }
        });


        final TranslateAnimation b = new TranslateAnimation(
                0, Graphics.getPixelsFromPercentFromX(80) * -1,
                0, 0);
        b.setDuration(SLEEP_TIMER_FOR_SLIDE_OUT_ANIMATION);
        b.setFillEnabled(true);
        b.setFillAfter(true); //HERE
        b.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
                Log.d("Events", "Menu button clicked, firing off animation");

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */
                    @Override
                    public void run() {
                        slideOutMenuWebView.setVisibility(View.VISIBLE);
                        slideOutMenuWebView.requestLayout();
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                Log.d("Events", "Menu button clicked, animation again");
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */
                    @Override
                    public void run() {
                        reassertSelfNaturalOrderingWVBehindMainCapturingState();
                        slideOutMenuWebView.setVisibility(View.GONE);
                        slideOutMenuWebView.requestLayout();
                    }
                });


                Log.d("Events", "Menu button clicked, animation ending");
            }
        });

        final TranslateAnimation a = new TranslateAnimation(
                -1 * Graphics.getPixelsFromPercentFromX(80), 0,
                0, 0);
        a.setDuration(SLEEP_TIMER_FOR_SLIDE_OUT_ANIMATION);
        a.setFillEnabled(true);
        a.setFillAfter(true); //HERE
        a.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
                Log.d("Events", "Menu button clicked, firing off animation");
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */
                    @Override
                    public void run() {
                        slideOutMenuWebView.setVisibility(View.VISIBLE);
                        slideOutMenuWebView.requestLayout();
                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                Log.d("Events", "Menu button clicked, animation again");
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                Log.d("Events", "Menu button clicked, animation ending");


                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */
                    @Override
                    public void run() {
                        slideOutMenuWebView.setX(0);
                        slideOutMenuWebView.setVisibility(View.VISIBLE);
                        //  slideOutMenuWebView.requestLayout();
                    }
                });
            }
        });


        menuIcon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Handler handler = new Handler(Looper.getMainLooper());


                if (slideOutMenuIsOut == true) {
                    slideOutMenuIsOut = false;
                } else if (slideOutMenuIsOut == false) {
                    slideOutMenuIsOut = true;
                }


                if (slideOutMenuIsOut == false) { //slidein
                    Log.d("Event", "Calling animation 2");
                    handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            slideOutMenuWebView.clearAnimation();
                            slideOutMenuWebView.setAnimation(b);
                            slideOutMenuWebView.setVisibility(View.VISIBLE);
                            slideOutMenuWebView.requestLayout();
                            slideOutMenuWebView.getAnimation().start();
                        }


                    });
                } else {  //slideout
                    Log.d("Event", "Calling animation 1");

                    handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        /**
                         * Starts executing the active part of the class' code. This method is
                         * called when a thread is started that has been created with a class which
                         * implements {@code Runnable}.
                         */
                        @Override
                        public void run() {
                            slideOutMenuWebView.setAlpha(1.0f);
                        }
                    });

                    handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            reassertSelfNaturalOrdering();
                            slideOutMenuWebView.clearAnimation();
                            slideOutMenuWebView.setAnimation(a);
                            slideOutMenuWebView.setVisibility(View.VISIBLE);
                            slideOutMenuWebView.requestLayout();
                            slideOutMenuWebView.getAnimation().start();
                        }
                    });

                }


                Log.d("Events", "Menu clicked");
                //actions
            }

        });

        myDefaultLayout.post(new Runnable() {

            @Override
            public void run() {
                Handler handler = new Handler(Looper.getMainLooper());

                handler.post(new Runnable() {
                    public void run() {
                        slideOutMenuWebView.setAlpha(0.0f);
                    }
                });

                if (Graphics.getDisplaySize().y <= 1776) {


                    //resizing

                    Log.d("Image threshhold", String.valueOf(ToolBarItems.IMAGE_SCALE_THRESHOLD));

                    notificationBox.getLayoutParams().width = (int) (notificationBox.getWidth() * ToolBarItems.IMAGE_SCALE_THRESHOLD);
                    notificationBox.getLayoutParams().height = (int) (notificationBox.getHeight() * ToolBarItems.IMAGE_SCALE_THRESHOLD);

                    signInButton.getLayoutParams().width = (int) (signInButton.getWidth() * ToolBarItems.IMAGE_SCALE_THRESHOLD);
                    signInButton.getLayoutParams().height = (int) (signInButton.getHeight() * ToolBarItems.IMAGE_SCALE_THRESHOLD);

                    shopButton.getLayoutParams().width = (int) (shopButton.getWidth() * ToolBarItems.IMAGE_SCALE_THRESHOLD);
                    shopButton.getLayoutParams().height = (int) (shopButton.getHeight() * ToolBarItems.IMAGE_SCALE_THRESHOLD);

                    contactButton.getLayoutParams().width = (int) (contactButton.getWidth() * ToolBarItems.IMAGE_SCALE_THRESHOLD);
                    contactButton.getLayoutParams().height = (int) (contactButton.getHeight() * ToolBarItems.IMAGE_SCALE_THRESHOLD);

                    cartButton.getLayoutParams().width = (int) (cartButton.getWidth() * ToolBarItems.IMAGE_SCALE_THRESHOLD);
                    cartButton.getLayoutParams().height = (int) (cartButton.getHeight() * ToolBarItems.IMAGE_SCALE_THRESHOLD);

                    //position
                    ToolBarItems.makeInCenter(shopButton);
                    ToolBarItems.makeInCenter(contactButton);
                    contactButton.setX(contactButton.getX() + Graphics.getPixelsFromPercentFromX(20));
                    ToolBarItems.makeInRight(cartButton);
                    ToolBarItems.setMargins(cartButton, 0, 0, Graphics.getPixelsFromPercentFromX(5), Graphics.getPixelsFromPercentFromY(ToolBarItems.DEFAULT_PERCENT_MARGIN_BOTTOM_AS_PERCENT));

                    ToolBarItems.makeInCenter(signInButton);
                    signInButton.setX(signInButton.getX() - Graphics.getPixelsFromPercentFromX(20));
                    //setting margins if different from default


                    //buttons that need refactoring set them to invisible here
                    notificationBox.setVisibility(View.INVISIBLE);
                }
            }
        });


        try {
            scanButton = Graphics.getImageView("Barcode.png", this);
            //myBackgroundForToolbar = Graphics.getImageView("skin.gif", this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Intent barCodeActivity = new Intent(this, BarCodeScan.class);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BarCodeScan.setStartScanIntegrator(true);
                startActivity(barCodeActivity);
            }
        });

        assert scanButton != null;
        //assert myBackgroundForToolbar != null;

        /*ToolBarItems.makeAtBottomForNewImageAndSettingNewLayout(myBackgroundForToolbar);
        myBackgroundForToolbar.getLayoutParams().height = Graphics.getPixelsFromPercentFromY(8);
        myBackgroundForToolbar.getLayoutParams().width = Graphics.getPixelsFromPercentFromX(100);
        ToolBarItems.makeSizedImageFitToContainerSize(myBackgroundForToolbar);*/


        ToolBarItems.makeAtRightForNewImageAndSettingNewLayout(scanButton);
        int increaseSizeOfScanButtonToBeClickableByAmountInPixels = 5;
        scanButton.getLayoutParams().height = Graphics.getPixelsFromPercentFromY(3) + increaseSizeOfScanButtonToBeClickableByAmountInPixels;
        scanButton.getLayoutParams().width = (int) (Graphics.getPixelsFromPercentFromY(3) + (Graphics.getPixelsFromPercentFromY(1) * .18750000017) + increaseSizeOfScanButtonToBeClickableByAmountInPixels);
        ToolBarItems.makeSizedImageFitToContainerSize(scanButton);
        ToolBarItems.setMargins(scanButton, 0, Graphics.getPixelsFromPercentFromY(2) + (int) (Graphics.getPixelsFromPercentFromY(1) * .4), Graphics.getPixelsFromPercentFromX(9) + (int) (Graphics.getPixelsFromPercentFromX(1) * .6), 0);

        slideOutMenuWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        myWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);


        myDefaultLayout.addView(menuIcon);
        myDefaultLayout.addView(slideOutMenuWebView); //position here for testing
        myDefaultLayout.addView(myWebView);
        myDefaultLayout.addView(mySearchView);
        myDefaultLayout.addView(scanButton);

        //toolbar
        //no longer needed. myDefaultLayout.addView(myBackgroundForToolbar);
        myDefaultLayout.addView(backgroundForToolbar);

        myDefaultLayout.addView(signInButton);
        myDefaultLayout.addView(shopButton);
        myDefaultLayout.addView(notificationBox);
        myDefaultLayout.addView(contactButton);
        myDefaultLayout.addView(cartButton);
        myDefaultLayout.addView(numberOfItemsInCartDisplayToUserAsCircle);


        if (Graphics.GLOBAL_ORIENTATION == 3 || Graphics.GLOBAL_ORIENTATION == 2) {
            (new Thread() {
                public void run() {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        /**
                         * Starts executing the active part of the class' code. This method is
                         * called when a thread is started that has been created with a class which
                         * implements {@code Runnable}.
                         */
                        @Override
                        public void run() {
                            Graphics.setWebViewDimensionsWithDimensions(myWebView, Graphics.getDisplaySize().x, Graphics.getDisplaySize().y);
                            myWebView.setY(0);
                            myWebView.setX(0);
                            myDefaultLayout.bringChildToFront(myWebView);
                        }
                    });
                }
            }).start();
        } else {
            myWebView.setY(offsetWebViewOnVerticleAxis);
        }

        UpdateCartForUserNotLoggedIn myUpdateAsyncTask = new UpdateCartForUserNotLoggedIn();
        myUpdateAsyncTask.execute();

        getWindow().getDecorView().setBackgroundColor(Color.parseColor(Graphics.HEX_COLOR_BACKGROUND_CESCO));
        this.setContentView(myDefaultLayout);

    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void setWebViewCache(WebView myWebView, Activity myWebActivity) {
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        myWebView.getSettings().setAppCachePath(String.valueOf(myWebActivity.getApplicationContext().getCacheDir()));
        myWebView.getSettings().setAppCacheEnabled(true);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        myWebView.getSettings().setDomStorageEnabled(true);
        myWebView.getSettings().setDatabaseEnabled(true);
        String packageName = "com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication";
        myWebView.getSettings().setDatabasePath("/data/data/" + packageName + "/databases");

    }

    private void setJavaScriptEnabledForWebView(WebView myWebView, boolean setting) {
        myWebView.getSettings().setJavaScriptEnabled(setting);
    }

    private void setCenterInParentRuleToMyWebView(WebView myWebView) {
        assert (myWebView != null);
        RelativeLayout.LayoutParams myWebViewLayoutParams = (RelativeLayout.LayoutParams) myWebView.getLayoutParams();
        if ((myWebViewLayoutParams == null)) {
            throw new AssertionError("Layout Parameters not given to the object");
        }
        myWebViewLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        myWebView.setLayoutParams(myWebViewLayoutParams);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save the state of the WebView
        myWebView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore the state of the WebView
        myWebView.restoreState(savedInstanceState);
    }

    private void alignToLeftAndBelowOffset(WebView myWebView, int topOffset) {
        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) myWebView.getLayoutParams();
        myParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        myParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        myParams.setMargins(0, topOffset, 0, 0);
        myWebView.setLayoutParams(myParams);
    }

    private void alignToLeftAndBelowOffsetOutsideScreen(WebView myWebView, int topOffset) {
        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) myWebView.getLayoutParams();
        myParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        myParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        myParams.setMargins(0, topOffset, 0, 0);
        myWebView.setLayoutParams(myParams);
    }

    private static class GetItemsInCartForLoggedInUserRestfulRequestGetAsResultJson extends AsyncTask {


        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Object doInBackground(Object[] params) {
            while (true) {
                try {
                    Thread.sleep(SLEEP_TIMER_FOR_REFRESHING_LOGGED_IN_USER_CART_IN_SECONDS * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String strippedString = null;
                try {
                    strippedString = readUrl(URL_LOGGED_IN_APP_COUNTER);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                assert strippedString != null;
                String finalStrippedString = null;
                try {
                    Log.d("Service", "json response unstripped:" + strippedString);
                    strippedString = strippedString.replaceFirst("\"", "");
                    strippedString = strippedString.replaceFirst("\"", "");
                    final int startOfStrip = strippedString.indexOf("\"");
                    strippedString = strippedString.replaceFirst("\"", "");
                    final int endOfStrip = strippedString.indexOf("\"");
                    finalStrippedString = (strippedString.substring(startOfStrip, endOfStrip));
                } catch (StringIndexOutOfBoundsException e) {
                    continue; //try again
                } catch (NullPointerException e) {
                    continue; //try again
                }
                Log.d("Service", "json response:" + finalStrippedString);
                Handler handler = new Handler(Looper.getMainLooper());

                final String finalStrippedString1 = finalStrippedString;
                handler.post(new Runnable() {
                    public void run() {
                        numberOfItemsInCartDisplayToUserAsCircle.setText(finalStrippedString1);
                    }
                });

            }
        }

    }

    private class UpdateCartForUserNotLoggedIn extends AsyncTask<Void, String, Void> {

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Void doInBackground(Void... params) {
            while (true) {
                int SLEEP_DURATION_FOR_UPDATING_NUMBERITEMSINCART_FOR_USER_NOT_LOGGED_IN = 2500; /*this is an upper bound -> we still have lock
                for best base scenario. This sleep timer is used to reduce networking requests and help the application by only requesting in a fair
                manner. In any event, not one networking thread should be allowed to starve the user's network connection/resource therefore...
                the sleep timer represents that upper bound, of which, we will at minimum wait, even if the request has completed.
                @see below semaphore
                */

                try {
                    lockForGettingCartDataForUserNotLoggedIn.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //sleep for N seconds
                try {
                    Thread.sleep(SLEEP_DURATION_FOR_UPDATING_NUMBERITEMSINCART_FOR_USER_NOT_LOGGED_IN);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */


                    @Override
                    public void run() {
                        if (MainActivity.getUsername().equals("") && MainActivity.getPassword().equals("")) {
                            Log.d("HTML", "Requesting(load) proxy site for cart update.");
                            myResourceWebViewForNotLoggedInUsers.loadUrl(CART_PAGE_FULL_SITE);
                        } else {
                            Log.d("Service", "Attempting to login and Sending a request");
                            myResourceWebViewForNotLoggedInUsers.loadUrl(URL_LOGIN.replace("userLogin=", "userLogin=" + MainActivity.getUsername()).replace("userPassword=", "userPassword=" + MainActivity.getPassword()));
                            try {
                                Log.d("Service", "json response:" + readUrl(URL_LOGGED_IN_APP_COUNTER));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            GetItemsInCartForLoggedInUserRestfulRequestGetAsResultJson continueJsonResponseOnThread = new GetItemsInCartForLoggedInUserRestfulRequestGetAsResultJson();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                continueJsonResponseOnThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                continueJsonResponseOnThread.execute();
                            }
                        }
                    }
                });
            }

        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Void result) {

        }

        protected void onProgressUpdate(String[] progress) {
            Log.d("HTML", String.valueOf(progress[progress.length - 1]));
        }

    }


    public class MyJavascriptCartInterface {
        @JavascriptInterface
        // process the html as needed by the app
        public synchronized void showHTML(String html) {
            Document document = Jsoup.parse(html);
            final String result = (document.getElementById("headerItemsCount").text() == null) ? "0" : document.getElementById("headerItemsCount").text();
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    numberOfItemsInCartDisplayToUserAsCircle.setText(result);
                }
            });
            Log.d("HTML", result);
        }
    }

    public class MyCheckboxValidator {
        @JavascriptInterface
        // check if there are checkboxes on the page
        public synchronized void showHTML(String html) {

            Log.d("HTML", "javascript checkbox checker running");

            Document document = Jsoup.parse(html);
            Elements elementSetOfAllInputs = document.getElementsByTag("input");
            List<Elements> checkboxesList = new ArrayList<Elements> ();
            for (Element element : elementSetOfAllInputs) {
                checkboxesList.add(element.getElementsByTag("checkbox"));
            }

            if (checkboxesList.isEmpty()) {
                Log.d("HTML", "No checkboxes detected for compare");
                pageDoesContainCheckBoxes = false;
            } else {
                Log.d("HTML", "Checkboxes have been detected for compare");
                pageDoesContainCheckBoxes = true;
            }
            Log.d("HTML", html);
        }
    }


    private void reassertSelfNaturalOrdering() {
        myDefaultLayout.bringChildToFront(menuIcon);
        myDefaultLayout.bringChildToFront(myWebView);
        myDefaultLayout.bringChildToFront(slideOutMenuWebView); //position here for testing
        myDefaultLayout.bringChildToFront(mySearchView);
        myDefaultLayout.bringChildToFront(scanButton);

        //toolbar
        myDefaultLayout.bringChildToFront(backgroundForToolbar);
        myDefaultLayout.bringChildToFront(signInButton);
        myDefaultLayout.bringChildToFront(shopButton);
        //  myDefaultLayout.bringChildToFront(notificationBox);
        myDefaultLayout.bringChildToFront(contactButton);
        myDefaultLayout.bringChildToFront(cartButton);
        myDefaultLayout.bringChildToFront(numberOfItemsInCartDisplayToUserAsCircle);
    }

    private void reassertSelfNaturalOrderingWVBehindMainCapturingState() {
        myDefaultLayout.bringChildToFront(menuIcon);
        myDefaultLayout.bringChildToFront(slideOutMenuWebView); //position here for testing
        myDefaultLayout.bringChildToFront(myWebView);
        myDefaultLayout.bringChildToFront(mySearchView);
        myDefaultLayout.bringChildToFront(scanButton);

        //toolbar
        myDefaultLayout.bringChildToFront(backgroundForToolbar);
        myDefaultLayout.bringChildToFront(signInButton);
        myDefaultLayout.bringChildToFront(shopButton);
        //  myDefaultLayout.bringChildToFront(notificationBox);
        myDefaultLayout.bringChildToFront(contactButton);
        myDefaultLayout.bringChildToFront(cartButton);
        myDefaultLayout.bringChildToFront(numberOfItemsInCartDisplayToUserAsCircle);
    }

    private void reassertToolbar() {
        //toolbar
        myDefaultLayout.bringChildToFront(backgroundForToolbar);
        myDefaultLayout.bringChildToFront(signInButton);
        myDefaultLayout.bringChildToFront(shopButton);
        //  myDefaultLayout.bringChildToFront(notificationBox);
        myDefaultLayout.bringChildToFront(contactButton);
        myDefaultLayout.bringChildToFront(cartButton);
        myDefaultLayout.bringChildToFront(numberOfItemsInCartDisplayToUserAsCircle);
    }

    private void hideToolbarAndBackdrop() {
        (backgroundForToolbar).setVisibility(View.INVISIBLE);
        (signInButton).setVisibility(View.INVISIBLE);
        (shopButton).setVisibility(View.INVISIBLE);
        //  (notificationBox).setVisibility(View.INVISIBLE);
        (contactButton).setVisibility(View.INVISIBLE);
        (cartButton).setVisibility(View.INVISIBLE);
        (numberOfItemsInCartDisplayToUserAsCircle).setVisibility(View.INVISIBLE);
    }

    private void showToolbarAndBackground() {
        (backgroundForToolbar).setVisibility(View.VISIBLE);
        (signInButton).setVisibility(View.VISIBLE);
        (shopButton).setVisibility(View.VISIBLE);
        // (notificationBox).setVisibility(View.VISIBLE);
        (contactButton).setVisibility(View.VISIBLE);
        (cartButton).setVisibility(View.VISIBLE);
        (numberOfItemsInCartDisplayToUserAsCircle).setVisibility(View.VISIBLE);
    }

}