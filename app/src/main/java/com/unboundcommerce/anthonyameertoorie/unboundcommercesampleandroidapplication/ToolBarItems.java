package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.IOException;

/**
 * Created by anthonyameertoorie on 6/30/15.
 *
 * Remember that this API layer is an abstraction of button.
 * If your implementation requires that you use layout->'post' because the view is created after
 * do so...
 */
public class ToolBarItems {
    public static final int DEFAULT_PERCENT_MARGIN_BOTTOM_AS_PERCENT = 2;
    private static Button notificationBox;
    private static Button signInButton;
    private static Button shopButton;
    private static Button contactButton;
    private static Button cartButton;

    //TODO: Move to graphics lib.
    //note that the max denom should represent scaling properties
    private static final double minThreshold = .75;
    public static double IMAGE_SCALE_THRESHOLD = (Graphics.getPixelsFromPercentFromX(100) / 1080.0) > minThreshold ?  (Graphics.getPixelsFromPercentFromX(58) / 1080.0) : minThreshold;
    //used to resize the images.
    private static final double minThresholdForImageViews = .75;
    private static final double minSizeScaleOfImagesForSmallerScreens = .65;
    public static double IMAGE_SCALE_THRESHOLD_FOR_IMAGEVIEWS = (Graphics.getPixelsFromPercentFromX(100) / 1080.0) > minThresholdForImageViews ?  (Graphics.getPixelsFromPercentFromX(150) / 1080.0) : minSizeScaleOfImagesForSmallerScreens;


    ToolBarItems() {
        if (Graphics.displaySize == null) {
            Log.e("Logical Error", "Toolbar not created when retaining x,y");
        }
    }

    public static void makeInCenter(Button centerButton) {
        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) centerButton.getLayoutParams();
        myParams.addRule(RelativeLayout.CENTER_IN_PARENT);
    }

    public static void makeAtBottom(Button centerButton) {
        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) centerButton.getLayoutParams();
        myParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
    }

    public static void makeAtBottomForNewImageAndSettingNewLayout(ImageView myView) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        myView.setLayoutParams(params);
    }

    public static void makeAtRightForNewImageAndSettingNewLayout(ImageView myView) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        myView.setLayoutParams(params);
    }

    public static void makeSizedImageFitToContainerSize(ImageView myView) {
        myView.setAdjustViewBounds(true);
        myView.setScaleType(ImageView.ScaleType.FIT_XY);
    }


    public static void makeInRight(Button centerButton) {
        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) centerButton.getLayoutParams();
        myParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
    }

    public static void setMargins(Button centerButton, int left, int top, int right, int bottom) {
        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) centerButton.getLayoutParams();
        myParams.setMargins(left, top, right, bottom);
    }

    public static void setMargins(ImageView centerButton, int left, int top, int right, int bottom) {
        RelativeLayout.LayoutParams myParams = (RelativeLayout.LayoutParams) centerButton.getLayoutParams();
        myParams.setMargins(left, top, right, bottom);
    }



    public static void constructToolbarItems(Activity workingFrame) throws IOException {
        notificationBox = setSizeAndImageOfToolbarButton(workingFrame, "notificationbox");
        signInButton = setSizeAndImageOfToolbarButton(workingFrame, "accountgrey");
        shopButton = setSizeAndImageOfToolbarButton(workingFrame, "shop");
        cartButton = setSizeAndImageOfToolbarButton(workingFrame, "cart");
        contactButton = setSizeAndImageOfToolbarButton(workingFrame, "contact");
    }

    public static Drawable getDrawable(String name, Activity workingFrame) {
        int resourceId = workingFrame.getResources().getIdentifier(name, "drawable", workingFrame.getApplicationContext().getPackageName());
        return workingFrame.getResources().getDrawable(resourceId);
    }


    private static Button setSizeAndImageOfToolbarButton(Activity workingFrame, String name) {
        Button myButton = new Button(workingFrame);

        //need these calls for some odd reason
        myButton.setHeight(1);
        myButton.setWidth(1);
        myButton.setMinimumWidth(1);
        myButton.setMinimumHeight(1);
        myButton.setMaxHeight(1);
        myButton.setMaxWidth(1);

        Graphics.alignButtonToBottomLeftWithRelativeViewWithMarginsFixed(myButton, true);

        int sdk = android.os.Build.VERSION.SDK_INT;

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            myButton.setBackgroundDrawable(getDrawable(name, workingFrame));
        } else {
            myButton.setBackground(getDrawable(name, workingFrame));
        }


        return myButton;
    }

    public static Button getNotificationBox() {
        return notificationBox;
    }

    public static Button getSignInButton() {
        return signInButton;
    }

    public static Button getShopButton() {
        return shopButton;
    }


    public static Button getContactButton() {
        return contactButton;
    }

    public static Button getCartButton() {
        return cartButton;
    }


}
