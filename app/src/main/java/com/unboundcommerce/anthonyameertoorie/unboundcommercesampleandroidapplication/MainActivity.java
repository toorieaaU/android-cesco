package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import static android.os.StrictMode.ThreadPolicy;
import static android.os.StrictMode.setThreadPolicy;


public class MainActivity extends ActionBarActivity {
    //constants
    private static final String DEFAULT_BUTTON_TEXT = "Login";
    private static final String DEFAULT_BUTTON_PRESSED = "...RELEASE...";
    private static final String SCAN_BUTTON_PRESSED = "Scan Product Code";
    private static RelativeLayout myDefaultLayout;
    private Button webViewPortalButton;
    private Button scanCodeButton;
    private EditText usernameTextInput;
    private EditText passwordTextInput;

    public static String getUsername() {
        return username;
    }

    public static String getPassword() {
        return password;
    }

    private static String username = "";
    private static String password = "";

    /* Image capturing */
    private Uri mImageUri;

    /* Image capturing */
    private File createTemporaryFile(String part, String ext, Activity context) throws Exception
    {
        File tempDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);


        //File tempDir= Environment.getExternalStorageDirectory();
        //tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    private Bitmap getBitmapBasedOnUri(Uri uri){
        //Reconstructing full size image based on URI using 'content resolver'
        this.getContentResolver().notifyChange(uri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap = null;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
            Log.d("Image", "Failed to load", e);
        }

        return bitmap;
    }

    /* Image capturing */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(requestCode==1888 && resultCode==RESULT_OK) {
            Log.d("Image", "Success");


            Bitmap bitmap = getBitmapBasedOnUri(mImageUri);

            Log.d("Image", bitmap.toString());
            Log.d("Image", String.valueOf(bitmap.getWidth()));
            Log.d("Image", String.valueOf(bitmap.getHeight()));
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        {
            Display display = getWindowManager().getDefaultDisplay();
            Point displaySize = new Point();
            display.getSize(displaySize);
            Graphics.setDisplaySize(displaySize);
            Log.d("Passing 'Point' Object", Integer.toString(displaySize.x));
        }
        Graphics.lockOrientation(this);
        super.onCreate(savedInstanceState);

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = null;
        try
        {
            // place where to store camera taken picture
            photo = this.createTemporaryFile("picture", ".jpg", this);
            //photo.delete();
        } catch (Exception e)
        {
            Log.v("Image", "Can't create file to take picture!");
            Log.v("Image", e.toString());
            Log.v("Image", e.getMessage());
            //Toast.makeText(this, "Please check SD card! Image shot is impossible!", Toast.LENGTH_LONG);
        }
        mImageUri = Uri.fromFile(photo); //even if the photo is deleted, the identifier is still valid!!!!
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        //start camera intent
        //this.startActivityForResult(intent, 1888);

        WebActivity.deleteAllCookies(this);

        //for JSON calls
        ThreadPolicy policy = new ThreadPolicy.Builder().permitAll().build();
        setThreadPolicy(policy);


        myDefaultLayout = new RelativeLayout(this);

        //styling and alloc
        usernameTextInput = new EditText(this);
        passwordTextInput = new EditText(this);

        // testing purposes only, remove in production
        usernameTextInput.setText("toorieaa@bu.edu");
        passwordTextInput.setText("SoulJah123");
        usernameTextInput.setTextColor(Color.BLACK);
        passwordTextInput.setTextColor(Color.BLACK);

        usernameTextInput.setBackgroundColor(Color.parseColor("#FFFFFF"));
        passwordTextInput.setBackgroundColor(Color.parseColor("#FFFFFF"));

        usernameTextInput.setWidth(Graphics.getPixelsFromPercentFromX(60));
        usernameTextInput.setHeight(Graphics.getPixelsFromPercentFromY(7));

        passwordTextInput.setWidth(Graphics.getPixelsFromPercentFromX(60));
        passwordTextInput.setHeight(Graphics.getPixelsFromPercentFromY(7));

        //positioning
        usernameTextInput.setY(Graphics.getPixelsFromPercentFromY(10));
        passwordTextInput.setY(Graphics.getPixelsFromPercentFromY(10 + 7 + 1));

        RelativeLayout.LayoutParams setToCenterParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        setToCenterParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        usernameTextInput.setLayoutParams(setToCenterParams);
        passwordTextInput.setLayoutParams(setToCenterParams);




        scanCodeButton = new Button(this);
        scanCodeButton.setText(SCAN_BUTTON_PRESSED);
        webViewPortalButton = new Button(this);

        webViewPortalButton.setText(DEFAULT_BUTTON_TEXT);
        webViewPortalButton.setWidth(Graphics.getPixelsFromPercentFromX(50));
        webViewPortalButton.setHeight(Graphics.getPixelsFromPercentFromY(10));
        scanCodeButton.setWidth(Graphics.getPixelsFromPercentFromX(50));
        scanCodeButton.setHeight(Graphics.getPixelsFromPercentFromY(10));

        webViewPortalButton.setBackgroundDrawable(Graphics.createGradiantDrawableWithBorderAndCurves(Resources.BUTTON_DEFAULT_COLOR, 25, 3, Graphics.HEX_COLOR_BLACK));
        scanCodeButton.setBackgroundDrawable(Graphics.createGradiantDrawableWithBorderAndCurves(Resources.BUTTON_DEFAULT_COLOR, 25, 3, Graphics.HEX_COLOR_BLACK));
        Graphics.setButtonTransparency(85, webViewPortalButton);
        Graphics.setButtonTransparency(85, scanCodeButton);

        myDefaultLayout.post(new Runnable() {
            @Override
            public void run() {
                Graphics.alignButtonToBottomLeft(webViewPortalButton);
                Graphics.setCenterPositionOfButtonOnXAxis(webViewPortalButton);
                Graphics.alignButtonToBottomLeft(scanCodeButton);
                Graphics.setCenterPositionOfButtonOnXAxis(scanCodeButton);

                Graphics.moveButtonUpByPercent(scanCodeButton, 12);
                //Graphics.setCenterPositionOfButtonOnYAxis(webViewPortalButton);
                //Graphics.alignButtonToBottomLeft(scanCodeButton);
            }
        });

        ImageView logo = null;
        try {
            logo = Graphics.getImageView("cesco.png", this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert logo != null;

        //TODO: Refactor in graphics module based on dimensions
        final float SCALE = Graphics.scaleBasedOnScreenDimensionsSmallImage();
        //Graphics.setImageLocationAndSize(logo,0,0,190*4,89*4);
        Graphics.setImageLocationCenterXAndSize(logo, Graphics.displaySize.x, Graphics.displaySize.y);


        final Intent barCodeScanActivity = new Intent(this, BarCodeScan.class);
        BarCodeScan.setStartScanIntegrator(false);
        scanCodeButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    scanCodeButton.setText(SCAN_BUTTON_PRESSED);
                    scanCodeButton.setBackgroundDrawable(Graphics.createGradiantDrawableWithBorderAndCurves(Resources.BUTTON_DEFAULT_COLOR, 25, 3, Graphics.HEX_COLOR_BLACK));
                    Graphics.setButtonTransparency(85, scanCodeButton);
                    startActivity(barCodeScanActivity);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    scanCodeButton.setBackgroundDrawable(Graphics.createGradiantDrawableWithBorderAndCurves(Resources.BUTTON_HOVER_COLOR, 25, 3, Graphics.HEX_COLOR_BLACK));
                    Graphics.setButtonTransparency(85, scanCodeButton);
                    scanCodeButton.setText(SCAN_BUTTON_PRESSED);
                    return true;
                }
                return false;
            }
        });

        final Intent webActivity = new Intent(this, WebActivity.class);
        webViewPortalButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    webViewPortalButton.setText(DEFAULT_BUTTON_TEXT);
                    webViewPortalButton.setBackgroundDrawable(Graphics.createGradiantDrawableWithBorderAndCurves(Resources.BUTTON_DEFAULT_COLOR, 25, 3, Graphics.HEX_COLOR_BLACK));
                    Graphics.setButtonTransparency(85, webViewPortalButton);
                    username = usernameTextInput.getText().toString();
                    password = passwordTextInput.getText().toString();
                    startActivity(webActivity);
                    return true;
                }
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    webViewPortalButton.setBackgroundDrawable(Graphics.createGradiantDrawableWithBorderAndCurves(Resources.BUTTON_HOVER_COLOR, 25, 3, Graphics.HEX_COLOR_BLACK));
                    Graphics.setButtonTransparency(85, webViewPortalButton);
                    webViewPortalButton.setText(DEFAULT_BUTTON_TEXT);
                    return true;
                }
                return false;
            }
        });


        myDefaultLayout.addView(logo);
        myDefaultLayout.addView(webViewPortalButton);

        //myDefaultLayout.addView(scanCodeButton);
        myDefaultLayout.addView(usernameTextInput);
        myDefaultLayout.addView(passwordTextInput);

        this.setContentView(myDefaultLayout);
    }

}
