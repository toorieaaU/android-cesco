package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;

/**
 * Created by anthonyameertoorie on 6/9/15.
 */
public class CustomWebChromeClient extends WebChromeClient {
    private static int MB_STORAGE = 128;
    private ProgressDialog progressDialog;
    private int endMessageIndex;

    CustomWebChromeClient(ProgressDialog progressDialog, int endMessageIndex) {
        this.progressDialog = progressDialog;
        this.endMessageIndex = endMessageIndex;
    }

    @Override
    public void onProgressChanged(final WebView view, final int progress) {

        if(this.endMessageIndex == -1){
            return ;
        }

        if (progress < 100) {
            Log.d("Progress", Integer.toString(progress));

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                /**
                 * Starts executing the active part of the class' code. This method is
                 * called when a thread is started that has been created with a class which
                 * implements {@code Runnable}.
                 */
                @Override
                public void run() {
                    progressDialog.setMessage("Progress:" + Integer.toString(progress) + "%");
                    try {
                        progressDialog.show();
                    }catch(WindowManager.BadTokenException e){
                        Log.w("Orientation","(redraw)Progress dialogue cannot be shown");
                    }catch(Exception e){
                        Log.w("Orientation","(redraw)Progress dialogue cannot be shown");
                    }
                }
            });
        }

        if (progress == 100) {
            Log.d("Progress", Integer.toString(progress));
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                /**
                 * Starts executing the active part of the class' code. This method is
                 * called when a thread is started that has been created with a class which
                 * implements {@code Runnable}.
                 */
                @Override
                public void run() {
                    progressDialog.setMessage("Progress:" + Integer.toString(progress) + "%");
                    try {
                        progressDialog.dismiss();
                    }catch(IllegalArgumentException e){
                        Log.w("Orientation","(redraw)Progress dialogue cannot be dismissed");
                    }
                }
            });
        }

    }

    @Override
    public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(MB_STORAGE * 1024 * 1024);
    }
}
