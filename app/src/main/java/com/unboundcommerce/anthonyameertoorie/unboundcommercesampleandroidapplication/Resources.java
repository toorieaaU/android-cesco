package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.graphics.Color;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by anthonyameertoorie on 6/8/15.
 */
public class Resources {
    private static final String JSON_URL = "https://api.myjson.com/bins/1dujg";
    //color themes
    public static String BUTTON_DEFAULT_COLOR = "WHITE";
    public static String BUTTON_HOVER_COLOR = "WHITE";

    //singleton
    public static final Resources MY_SINGLETON = new Resources();

    private Resources() {
        String target = Resources.getJSON(JSON_URL);
        try {
            Resources.BUTTON_DEFAULT_COLOR = Graphics.returnHexFromIntColor(Color.parseColor(Resources.getJsonParsedSingleObject(target, "style", "buttonColor")));
            Log.d("Defaulting color", Resources.BUTTON_DEFAULT_COLOR);
        } catch (JSONException e) {
            Log.e("Defaulting color", Resources.BUTTON_DEFAULT_COLOR);
        }

        try {
            Resources.BUTTON_HOVER_COLOR = Graphics.returnHexFromIntColor(Color.parseColor(Resources.getJsonParsedSingleObject(target, "style", "buttonColorHover")));
            Log.d("Defaulting color", Resources.BUTTON_HOVER_COLOR);
        } catch (JSONException e) {
            Log.e("Defaulting color", Resources.BUTTON_HOVER_COLOR);
        }
    }

    private static String getJSON(String address) {
        StringBuilder builder = new StringBuilder();
        HttpClient client;
        client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(address);
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                reader.close();
                content.close();

            } else {
                Log.e(MainActivity.class.toString(), "Failed JSON object");
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    private static String getJsonParsedSingleObject(String response, String object, String target) throws JSONException {
        JSONObject obj = new JSONObject(response);
        String jsonString = obj.getJSONObject(object).getString(target);
        return jsonString;
    }

    private static String getJsonParsedArrayElements(String response, String arrayName, String target, int index) throws JSONException {
        JSONObject obj = new JSONObject(response);
        JSONArray arr = obj.getJSONArray(arrayName);
        String arrayEntity = arr.getJSONObject(index).getString(target);
        return arrayEntity;
    }
}
