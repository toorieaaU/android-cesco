package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

/**
 * Created by anthonyameertoorie on 8/18/15.
 * This module dependency should not rely on a simulator.
 *
 * Proof of concept : Simulators simulate hardware using software mechanisms, leaving
 * the scope of error too large.
 * It accesses phone hardware that should not be simulated.
 * Orientation also falls into this category.
 *
 */
public class CameraAndEmail {


}
