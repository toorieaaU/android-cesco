package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Created by anthonyameertoorie on 6/4/15.
 */
public class BarCodeScan extends ActionBarActivity {
    private static final String APPEND_URL_CLIENT = "/?fromClient=true";
    private static String searchStringQuery = "";
    private static boolean startScanIntegrator = false;
    final IntentIntegrator scanIntegrator = new IntentIntegrator(this);
    private static RelativeLayout myDefaultLayout;
    private WebView dataLookupView;
    private String URL_SEARCH_DOMAIN = "http://stage.unboundcommerce.com/gstore/cesco/search/N=0&Ntk=defaultSearch&Ntx=mode+matchallpartial&Nty=1&Ntt=?*/?fromClient=true";
    public static volatile ProgressDialog mProgressDialog;
    private SearchView mySearchView;
    private Button backButton;

    public static boolean isStartScanIntegratorOn() {
        return startScanIntegrator;
    }

    public static void setStartScanIntegrator(boolean startScanIntegrator) {
        BarCodeScan.startScanIntegrator = startScanIntegrator;
    }

    public static String getSearchStringQuery() {
        return searchStringQuery;
    }

    public static void setSearchStringQuery(String funcSearchStringQuery) {
        searchStringQuery = funcSearchStringQuery;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        {
            Display display = getWindowManager().getDefaultDisplay();
            Point displaySize = new Point();
            display.getSize(displaySize);
            Graphics.setDisplaySize(displaySize);
            Log.d("Passing 'Point' Object", Integer.toString(displaySize.x));
        }

        super.onCreate(savedInstanceState);
        Graphics.lockOrientation(this);

        if(WebActivity.lockForGettingCartDataForUserNotLoggedIn.availablePermits() == 0){
            WebActivity.lockForGettingCartDataForUserNotLoggedIn.release();
        }
        myDefaultLayout = new RelativeLayout(this);

        final int offsetYForScannerView = Graphics.getPixelsFromPercentFromY(8);
        mProgressDialog = new ProgressDialog(this);



        dataLookupView = new WebView(this);
        WebActivity.webViewWillRetainWithinApp(dataLookupView, new WebViewClientCustomized(mProgressDialog, URL_SEARCH_DOMAIN, APPEND_URL_CLIENT, offsetYForScannerView, this));
        dataLookupView.setWebChromeClient(new CustomWebChromeClient(mProgressDialog, 20));
        Graphics.setWebViewDimensionsWithDimensions(dataLookupView, Graphics.getDisplaySize().x, Graphics.getDisplaySize().y - (int) (offsetYForScannerView * 2.5));
        setJavaScriptEnabledForWebView(dataLookupView, true);
        Graphics.setDefaultWebViewToLoadAllAssetsOnScreen(dataLookupView, true);
        //setCenterInParentRuleToMyWebView(dataLookupView);
        setWebViewCache(dataLookupView, this);
        dataLookupView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        dataLookupView.loadUrl(URL_SEARCH_DOMAIN.replaceFirst("\\?", getSearchStringQuery()));

        myDefaultLayout.post(new Runnable() {
            @Override
            public void run() {
                dataLookupView.setY(offsetYForScannerView);
            }
        });

        mySearchView = new SearchView(this);
        Graphics.setFontColorOfSearchView(mySearchView, Color.BLACK);
        mySearchView.setBackgroundColor(Color.WHITE);
        RelativeLayout.LayoutParams layoutparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        mySearchView.setLayoutParams(layoutparams);
        mySearchView.getLayoutParams().width = Graphics.getPixelsFromPercentFromX(96);
        mySearchView.getLayoutParams().height = Graphics.getPixelsFromPercentFromY(6);
        mySearchView.setX(Graphics.getPixelsFromPercentFromX(2));
        mySearchView.setY(Graphics.getPixelsFromPercentFromY(1));
        mySearchView.setFocusable(true);
        mySearchView.setIconifiedByDefault(false);
        mySearchView.clearFocus();

        mySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                String tmpQueryParser = query.replaceAll(" ", "%20");
                dataLookupView.loadUrl(URL_SEARCH_DOMAIN.replaceFirst("\\?", tmpQueryParser));
                //TODO : Disable search while loading view and only able to input after the view is visible.
                return true;

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        });

        backButton = new Button(this);
        backButton.setText("< Back");
        backButton.setBackgroundColor(Color.TRANSPARENT);
        backButton.setTextSize(20);
        backButton.setTextColor(Color.parseColor("WHITE"));

        final Intent webActivityMain = new Intent(this, WebActivity.class);

        backButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                    if (dataLookupView.canGoBack()) {
                        dataLookupView.goBack();
                    } else {
                        startActivity(webActivityMain);
                    }
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    return true;
                }
                return false;
            }
        });

        RelativeLayout.LayoutParams styleForBackButton = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        styleForBackButton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        styleForBackButton.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        backButton.setLayoutParams(styleForBackButton);

        myDefaultLayout.addView(dataLookupView);
        myDefaultLayout.addView(mySearchView);
        myDefaultLayout.addView(backButton);


        myDefaultLayout.setBackgroundColor(Color.parseColor(Graphics.HEX_COLOR_BACKGROUND_CESCO));


        this.setContentView(myDefaultLayout);
        if (isStartScanIntegratorOn()) {
            scanIntegrator.initiateScan();
            BarCodeScan.setStartScanIntegrator(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        Log.d("Format", String.valueOf(requestCode));
        if (scanningResult != null) {
            //we have a result
            String scanContentString = scanningResult.getContents();
            String scanFormatString = scanningResult.getFormatName();
            if(scanContentString == null){
                Log.d("Content", "null");
            }else {
                Log.d("Content", scanContentString);
            }

            if(scanFormatString == null){
                Log.d("Format", "null");
            }else {
                Log.d("Format", scanFormatString);
            }
            if (scanContentString != null) {
                dataLookupView.loadUrl(URL_SEARCH_DOMAIN.replaceFirst("\\?", scanContentString));
            }

        } else {
            Log.e("Content", "scan error");
            Log.e("Format", "scan error");
        }
    }

    private void setJavaScriptEnabledForWebView(WebView myWebView, boolean setting) {
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(setting);
    }

    private void setWebViewCache(WebView myWebView, Activity myWebActivity) {
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        myWebView.getSettings().setAppCachePath(String.valueOf(myWebActivity.getApplicationContext().getCacheDir()));
        myWebView.getSettings().setAppCacheEnabled(true);
        myWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        myWebView.getSettings().setDomStorageEnabled(true);
        myWebView.getSettings().setDatabaseEnabled(true);
        String packageName = "com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication";
        myWebView.getSettings().setDatabasePath("/data/data/" + packageName + "/databases");

    }

    private void setCenterInParentRuleToMyWebView(WebView myWebView) {
        assert (myWebView != null);
        RelativeLayout.LayoutParams myWebViewLayoutParams = (RelativeLayout.LayoutParams) myWebView.getLayoutParams();
        if ((myWebViewLayoutParams == null)) {
            throw new AssertionError("Layout Parameters not given to the object");
        }
        myWebViewLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        myWebView.setLayoutParams(myWebViewLayoutParams);
    }

}