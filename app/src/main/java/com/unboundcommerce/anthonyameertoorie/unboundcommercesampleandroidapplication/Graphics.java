package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Anthony Ameer Toorie on 6/2/15.
 * This graphics library leverages a very powerful, deprecated layout
 * called Relative Layout.
 * <p/>
 * This gives web developors the ability to program within the android SDK
 * using some of the same principles ie. (Absolute positioning, and relative positioning,
 * Resolution adaptation and more...)
 * ------------------------------------------------------------------------
 */
public class Graphics {
    public static final String HEX_COLOR_LIGHT_YELLOW = "#FAEBD7";
    public static final String HEX_COLOR_LIGHT_WHITE = "#FFFFFF";
    public static final String HEX_COLOR_BLACK = "#000000";
    public static final String HEX_COLOR_BACKGROUND_CESCO = "#023B66";
    public static int GLOBAL_ORIENTATION = 1; // 1, portrait. 2,3 landscape

    //global object that retains width and height
    public static Point displaySize;

    /*
    Start UI Block
    */

    public static Point getDisplaySize() {
        return displaySize;
    }

    public static void setDisplaySize(Point displaySize) {
        Graphics.displaySize = displaySize;
    }

    @Deprecated
    public static int getCenterPositionOfButtonOnXAxis(int thisButtonWidth) {
        return (displaySize.x / 2) - (thisButtonWidth / 2);
    }

    @Deprecated
    public static int getCenterPositionOfButtonOnYAxis(int thisButtonHeight) {
        int scalar = thisButtonHeight / 100;
        return (displaySize.y / 2) - ((thisButtonHeight / 2) + thisButtonHeight / scalar);
    }

    @Deprecated
    public static int getCenterPositionOfButtonOnXAxis(Button thisAndroidButton) {
        return (displaySize.x / 2) - (thisAndroidButton.getWidth() / 2);
    }

    @Deprecated
    public static int getCenterPositionOfButtonOnYAxis(Button thisAndroidButton) {
        int scalar = thisAndroidButton.getHeight() / 100;
        return (displaySize.y / 2) - ((thisAndroidButton.getHeight() / 2) + thisAndroidButton.getHeight() / scalar);
    }

    public static void setCenterPositionOfButtonOnXAxis(Button thisAndroidButton) {
        thisAndroidButton.setX((displaySize.x / 2) - (thisAndroidButton.getWidth() / 2));
    }

    public static void setCenterPositionOfButtonOnYAxis(Button thisAndroidButton) {
        int adjustment = 0;
        if (displaySize.y <= 800) {
            adjustment = 40;
        } else if (displaySize.y <= 1280) {
            adjustment = 100;
        } else if (displaySize.y <= 1920) {
            adjustment = 120;
        }

        int yPosition = (displaySize.y / 2) - (thisAndroidButton.getHeight() / 2);
        thisAndroidButton.setY(yPosition - adjustment);
        //Log.d("Height", Integer.toString(displaySize.y / 2));
        //Log.d("Height of Button", Integer.toString(thisAndroidButton.getHeight() / 2));
    }

    /**
     * This method should only be used with icon / small images
     * In the future we can refactor this function, with enough testing
     * to scale for all types of images
     *
     * @return an int value that represents the amount of magnification needed
     */
    public static float scaleBasedOnScreenDimensionsSmallImage() {
        float adjustment = 0;
        if (displaySize.y <= 800) {
            adjustment = 1f;
        } else if (displaySize.y <= 1280) {
            adjustment = 1.5f;
        } else if (displaySize.y <= 1920) {
            adjustment = 2f;
        }
        return adjustment;
    }


    //Loads image from assets
    //returns ImageView given string location
    public static ImageView getImageView(String location, Activity main) throws IOException {
        ImageView myLogoView = new ImageView(main);
        InputStream ims = null;
        try {
            ims = main.getAssets().open(location);
        } catch (IOException e) {
            Log.e("ImageView error", e.getStackTrace().toString());
        }
        // load image as Drawable
        Drawable d = Drawable.createFromStream(ims, null);
        // set image to ImageView
        myLogoView = new ImageView(main);
        myLogoView.setImageDrawable(d);
        return myLogoView;
    }

    public static void setImageLocationAndSize(ImageView myTarget, int x, int y, int width, int height) {
        //specify XY absolute
        myTarget.setScaleType(ImageView.ScaleType.FIT_XY);

        //set stretch
        myTarget.setMinimumWidth(width);
        myTarget.setMaxWidth(width);
        myTarget.setMinimumHeight(height);
        myTarget.setMaxHeight(height);

        //positions
        myTarget.setX(x);
        myTarget.setY(y);


        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width, height);
        myTarget.setLayoutParams(layoutParams);

    }

    public static int getPixelsFromPercentFromX(int percent) {
        return (displaySize.x * percent) / 100;
    }

    public static int getPixelsFromPercentFromY(int percent) {
        return (displaySize.y * percent) / 100;
    }

    public static void setImageLocationCenterXAndSize(ImageView myTarget, int width, int height) {
        //specify XY absolute
        myTarget.setScaleType(ImageView.ScaleType.FIT_XY);

        //set stretch
        myTarget.setMinimumWidth(width);
        myTarget.setMaxWidth(width);
        myTarget.setMinimumHeight(height);
        myTarget.setMaxHeight(height);


        //positions
        myTarget.setX(displaySize.x / 2 - (width / 2));
        myTarget.setY(0);
    }


    public static void moveButtonUpByPercent(Button myButton, int percent) {
        myButton.setY(myButton.getY() - Graphics.getPixelsFromPercentFromY(percent));
    }

    public static GradientDrawable createGradiantDrawableWithBorderAndCurves(String backgroundColor, int curveDepth, int borderSize, String borderColor) {
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(Color.parseColor(backgroundColor)); //background color
        gd.setCornerRadius(curveDepth); //curves
        gd.setStroke(borderSize, Color.parseColor(borderColor)); //border size and color

        return gd;
    }

    public static void setButtonToOrangeCircleWithColorAndSize(Button applyingButton, int size, String color) {
        RelativeLayout.LayoutParams myLayoutCustomForOrangePane = new RelativeLayout.LayoutParams(size, size);
        applyingButton.setLayoutParams(myLayoutCustomForOrangePane);
        applyingButton.setBackgroundDrawable(Graphics.createGradiantDrawableWithBorderAndCurves(color, 50, 3, Graphics.HEX_COLOR_BLACK));
    }

    @Deprecated
    public static void alignButtonToBottomLeft(Button myButton) {

        Log.d("Height", String.valueOf(myButton.getHeight()));
        int adjustment = 0;


        // if(myButton.getHeight() > 38){
        //     adjustment = myButton.getHeight() - 38;
        //}
        myButton.setY(Graphics.displaySize.y - myButton.getHeight() * 3 + adjustment);
    }

    /*
    @see ToolBarItems.java
     */
    public static void alignButtonToBottomLeftWithRelativeViewWithMarginsFixed(Button myButton, boolean marginsOn) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        if (marginsOn) {
            params.setMargins(0, 0, 0, Graphics.getPixelsFromPercentFromY(ToolBarItems.DEFAULT_PERCENT_MARGIN_BOTTOM_AS_PERCENT));
        }
        myButton.setLayoutParams(params);
    }

    public static void sendViewToBack(final View child) {
        final ViewGroup parent = (ViewGroup) child.getParent();
        if (null != parent) {
            parent.removeView(child);
            parent.addView(child, 0);
        }
    }

    public static void alignButtonToRight(Button myButton) {
        myButton.setX(Graphics.displaySize.x - myButton.getWidth() * 1);
    }

    public static void alignButtonToBottomRight(Button myButton) {
        alignButtonToBottomLeft(myButton);
        alignButtonToRight(myButton);
    }

    public static void lockOrientation(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        int rotation = display.getRotation();
        int height;
        int width;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
            height = display.getHeight();
            width = display.getWidth();
        } else {
            Point size = new Point();
            display.getSize(size);
            height = size.y;
            width = size.x;
        }
        switch (rotation) {
            case Surface.ROTATION_90:
                if (width > height)
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                else
                    activity.setRequestedOrientation(9/* reversePortait */);
                break;
            case Surface.ROTATION_180:
                if (height > width)
                    activity.setRequestedOrientation(9/* reversePortait */);
                else
                    activity.setRequestedOrientation(8/* reverseLandscape */);
                break;
            case Surface.ROTATION_270:
                if (width > height)
                    activity.setRequestedOrientation(8/* reverseLandscape */);
                else
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
            default :
                if (height > width)
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                else
                    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    public static void setButtonTransparency(int transparencyAlpha, Button myButton) {
        myButton.getBackground().setAlpha(transparencyAlpha);
    }

    public static String returnHexFromIntColor(int intColor) {
        String strColor = String.format("#%06X", 0xFFFFFF & intColor);
        return strColor;
    }
    /*
    @Deprecated
    public static void rotateWebView(float degree, View uiObject) {

        //Function has flaws because children (such as scrollviews) do not inherit
        //the rotation.

        final RotateAnimation rotateAnim = new RotateAnimation(0.0f, degree,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnim.setDuration(0);
        rotateAnim.setFillEnabled(true);
        rotateAnim.setFillAfter(true);
        uiObject.startAnimation(rotateAnim);
    } */


    public static void rotateWebView(float degree, WebView uiObject) {
        final ObjectAnimator rotateAnim = ObjectAnimator.ofFloat(uiObject, "rotation", degree);

        rotateAnim.setDuration(0);
        rotateAnim.start();
    }

    public static boolean rotateUIElementOnSuccess(float degree, View uiObject) {
        // see also very useful animation functions
        //myWebView.animate().scaleX(floatScaleX).scaleY(floatScaleY).setDuration(750).start();
        //myWebView.animate().translationX(0).translationY((Graphics.getStatusBarHeight(dynamicStateWebActivity) * -1) + Graphics.getPixelsFromPercentFromY(1)).setDuration(0).start();

        /*Matrix a = new Matrix();
        a.set(uiObject.getMatrix());
        Log.d("Orientation", a.toShortString());
        Matrix b = new Matrix();
        b.set(uiObject.getMatrix());
        b.postRotate(degree);*/

        try {
            final ObjectAnimator rotateAnim = ObjectAnimator.ofFloat(uiObject, "rotation", degree);
            rotateAnim.setDuration(0);
            rotateAnim.start();
        } catch (Exception e) {
            return false; //fail
        }


        /*Log.d("Orientation", uiObject.getMatrix().toShortString());
        Log.d("Orientation", b.toShortString());
        uiObject.getMatrix().set(a);
        Log.d("Orientation", uiObject.getMatrix().toShortString());*/


        return true; //success
    }

    public static int getStatusBarHeight(Activity activity) {
        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void setFontColorOfSearchView(SearchView mySearchView, int myColor){
        int idTextForSV = mySearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) mySearchView.findViewById(idTextForSV);
        textView.setTextColor(myColor);
    }

    /*
    Start Web View Block
     */
    public static void setWebViewDimensionsWithMaxHeight(WebView myWebView, int width) {
        myWebView.setLayoutParams(new RelativeLayout.LayoutParams(width, RelativeLayout.LayoutParams.FILL_PARENT));
    }

    public static void setWebViewDimensionsWithDimensions(WebView myWebView, int width, int height) {
        myWebView.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
    }

    public static void setDefaultWebViewToLoadAllAssetsOnScreen(WebView myWebView, boolean setting) {
        myWebView.getSettings().setLoadWithOverviewMode(setting);
        myWebView.getSettings().setUseWideViewPort(setting);
    }

    private static class MyAnimation extends Animation {
        private Matrix matrix;

        public MyAnimation(Matrix matrix) {
            this.matrix = matrix;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            t.getMatrix().set(matrix);
        }
    }
}