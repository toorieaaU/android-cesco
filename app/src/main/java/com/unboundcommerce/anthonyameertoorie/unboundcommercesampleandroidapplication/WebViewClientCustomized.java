package com.unboundcommerce.anthonyameertoorie.unboundcommercesampleandroidapplication;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;


/**
 * Created by anthonyameertoorie on 6/9/15.
 */
public class WebViewClientCustomized extends WebViewClient {
    private static Semaphore lock = new Semaphore(1);
    private static TranslateAnimation translation;
    private static ActionBarActivity WebActivityRef;
    //webview static UI/resource refs
    private static volatile WebView mainWebViewReference;
    private static volatile WebView navBarReference;
    private static volatile WebView resourceForNotLoggedInUser;
    //timers
    private static int TIME_TO_SLEEP_BEFORE_CHECKING_HTML = 1500;
    private final int delay = 750;
    private final Interpolator myTransition = new DecelerateInterpolator();
    private ProgressDialog progressDialog;
    private String urlHome;
    private String appendUrlClientInjected;
    private float originalPosition = 0;

    final private static RefreshLoginStatusForUser refreshLoginCookie = new RefreshLoginStatusForUser();
    private static boolean startRefreshTimerOnlyOnce = true;


    //array list for all URLS loaded
    final public static List <String>allUrlsThatHaveBeenLoadedInList = new ArrayList<String>();


    public static String getCookies() {
        return cookies;
    }

    static volatile String cookies;


    WebViewClientCustomized(ProgressDialog progressDialog, String urlHome, String appendUrlClient, float originalPosition, ActionBarActivity ref) {
        this.progressDialog = progressDialog;
        this.urlHome = urlHome;
        this.appendUrlClientInjected = appendUrlClient;
        this.originalPosition = originalPosition;
        WebActivityRef = ref;
    }

    public static void setResourceForNotLoggedInUser(WebView resourceForNotLoggedInUser) {
        WebViewClientCustomized.resourceForNotLoggedInUser = resourceForNotLoggedInUser;
    }

    public static void setMainWebViewReference(WebView mainWebViewReference) {
        WebViewClientCustomized.mainWebViewReference = mainWebViewReference;
    }

    public static void setNavBarReference(WebView navBarReference) {
        WebViewClientCustomized.navBarReference = navBarReference;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        // TODO Auto-generated method stub
        super.onPageStarted(view, url, favicon);
        Log.d("URL", "start load" + url);
        //Log.d("URL", "start load" + view.getUrl());
        //Log.d("URL", "start load" +  view.getOriginalUrl());
    }

    @Override
    public boolean shouldOverrideUrlLoading(final WebView view, String url) {

        //if the view is equal to the resource module for not logged in user
        //do not attempt to override the url. Only attempt to dispatch the request to load asap.
        Log.d("URL", view.getTitle());

        if (mainWebViewReference != null) {
            if (view.equals(mainWebViewReference)) {
                Log.d("WebView", "exists and is the same as the current WV");

            }else{
                Log.d("WebView", "is not same as main WV");
            }
        }else{
            Log.d("WebView", "broken WV. Repair needed");
        }

        if (mainWebViewReference != null) {

            if(url.contains("compareCescoNumbers")){
                mainWebViewReference.loadUrl(url + "&fromClient=true");
                return true;
            }

            //for menu drilled to the last category type page
            if(url.contains("#") && view.equals(mainWebViewReference)){
                view.loadUrl(url.replace("#","?fromClient=true"));
                return true;
            }
            //turning the user sort into the mobile version
            if(url.contains("?userSelectedSort") && view.equals(mainWebViewReference)){
                view.loadUrl(url.replace("?userSelectedSort", "?fromClient=true&userSelectedSort"));
                return true;
            }
            //grid views to mobile :: allowed
            if(url.contains("view=grid")){
                return false;
            }

            if (!url.equals(urlHome) && view.equals(mainWebViewReference)) {
                view.loadUrl(url + appendUrlClientInjected);
                progressDialog.setTitle("Loading");
                progressDialog.show();
                return true;
            } else if (url.equals(WebActivity.URL_HOME) && view.equals(mainWebViewReference)) {
                return false;
            }
        }

        if (mainWebViewReference != null) {
            if (view.equals(mainWebViewReference) && view.getUrl().equals(WebActivity.URL_CONTACT)) {
                Log.d("Contact", "on");
                return false;
            }
        }

        //do not forget the data processing webview
        if (navBarReference != null) {
            if (view.equals(navBarReference)) { //refactor, ids don't make sense, inject each webview
                String urlMenu = urlHome;
                if (url.equals(urlMenu)) {
                    return false;
                } else {
                    if (!url.contains("?")) {
                        mainWebViewReference.loadUrl(url + "?fromClient=true");
                    } else {
                        mainWebViewReference.loadUrl(url + "&fromClient=true");
                    }

                    Log.d("NavBar checking url", url);
                    return true;
                }
            }
        }
        return true;
    }

    @Override
    public void onPageFinished(final WebView view, String url) {
        int sdk = android.os.Build.VERSION.SDK_INT;

        Log.d("URL", "finished" + url);

        if(view.equals(mainWebViewReference)) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                /**
                 * Starts executing the active part of the class' code. This method is
                 * called when a thread is started that has been created with a class which
                 * implements {@code Runnable}.
                 */
                @Override
                public void run() {
                    //mainWebViewReference.stopLoading();
                }
            });
        }

        if(view.equals(mainWebViewReference)) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                /**
                 * Starts executing the active part of the class' code. This method is
                 * called when a thread is started that has been created with a class which
                 * implements {@code Runnable}.
                 */
                @Override
                public void run() {
                    mainWebViewReference.loadUrl("javascript:window.checkboxValidator.showHTML" +
                            "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                }
            });
        }

        if (view.equals(mainWebViewReference)) {
            allUrlsThatHaveBeenLoadedInList.add(url); //add the url to all the global list

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                /**
                 * Starts executing the active part of the class' code. This method is
                 * called when a thread is started that has been created with a class which
                 * implements {@code Runnable}.
                 */
                @Override
                public void run() {
                    //TODO: Fix styling.
                    //String jsStyle = "document.getElementsByTagName(\"body\")[0].style.marginBottom = '50px';document.body.style.padding = '0';";
                    //mainWebViewReference.loadUrl("javascript:" + jsStyle);
                    //mainWebViewReference.evaluateJavascript();
                }
            });

            if(Graphics.GLOBAL_ORIENTATION != 1 && url.contains("compareCescoNumbers")) {
                //mainWebViewReference.loadUrl("javascript:" + "document.body.style.zoom = \"70%\"");
                handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */
                    @Override
                    public void run() {
                        mainWebViewReference.setInitialScale(1);
                        mainWebViewReference.getSettings().setUseWideViewPort(true);
                        mainWebViewReference.getSettings().setLoadWithOverviewMode(true);
                       /* mainWebViewReference.loadUrl("javascript:" + "var metaTag=document.createElement('meta');\n" +
                               "metaTag.name = \"viewport\"\n" +
                              "metaTag.content = \"width=1000, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\"\n" +
                            "document.getElementsByTagName('head')[0].appendChild(metaTag);");*/
                    }
                });
            }
        }

        if(Graphics.GLOBAL_ORIENTATION != 1) {
            if (sdk >= Build.VERSION_CODES.KITKAT) {
                mainWebViewReference.evaluateJavascript("goCompare();", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        Log.d("Javascript", s); // Prints: "this"
                    }
                });
            }else{
                mainWebViewReference.loadUrl("javascript: goCompare();");
            }
        }

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            /**
             * Starts executing the active part of the class' code. This method is
             * called when a thread is started that has been created with a class which
             * implements {@code Runnable}.
             */
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
            }
        });
        //display loaded URL
        if(view.equals(mainWebViewReference)) {
            Log.d("URL", url);
        }


        if (resourceForNotLoggedInUser != null) {

            if (view.equals(resourceForNotLoggedInUser)) {
                //wait then update

                if(MainActivity.getUsername().equals("") && MainActivity.getPassword().equals("")){
                    Log.d("Service", "Not logged in, fetching items from cart");
                    Log.d("HTML Service", "SPAWNING THREAD TO GET CART INFO FOR NON LOGGED IN USERS");

                final WaitBeforeLookingAtJavascriptForNonLoggedInUser refreshDataFromCartNotLoggedIn = new WaitBeforeLookingAtJavascriptForNonLoggedInUser();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        refreshDataFromCartNotLoggedIn.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        refreshDataFromCartNotLoggedIn.execute();
                    }
                } else {
                    if(startRefreshTimerOnlyOnce) {
                        Log.d("Service", "(Logged in?)Trying " + url);
                        cookies = CookieManager.getInstance().getCookie(url);
                        Log.d("Service", "All the cookies in a string:" + cookies);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
                            refreshLoginCookie.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }else {
                            refreshLoginCookie.execute();
                        }
                        startRefreshTimerOnlyOnce = false;
                    }
                }
            }
        }

        if (url.equals(WebActivity.URL_HOME)) {
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {//way of using drawables using older sdks [compatability]
                ToolBarItems.getShopButton().setBackgroundDrawable(ToolBarItems.getDrawable("shopblue", WebActivityRef));
                ToolBarItems.getContactButton().setBackgroundDrawable(ToolBarItems.getDrawable("contact", WebActivityRef));
                ToolBarItems.getCartButton().setBackgroundDrawable(ToolBarItems.getDrawable("cart", WebActivityRef));
                ToolBarItems.getSignInButton().setBackgroundDrawable(ToolBarItems.getDrawable("accountgrey", WebActivityRef));
            } else {                //set other buttons back to 'inactive' UI state for older sdks
                ToolBarItems.getShopButton().setBackground(ToolBarItems.getDrawable("shopblue", WebActivityRef));
                ToolBarItems.getContactButton().setBackground(ToolBarItems.getDrawable("contact", WebActivityRef));
                ToolBarItems.getCartButton().setBackground(ToolBarItems.getDrawable("cart", WebActivityRef));
                ToolBarItems.getSignInButton().setBackground(ToolBarItems.getDrawable("accountgrey", WebActivityRef));
            }
        } else if (url.equals(WebActivity.URL_CONTACT)) {
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                ToolBarItems.getContactButton().setBackgroundDrawable(ToolBarItems.getDrawable("contactblue", WebActivityRef));
                ToolBarItems.getShopButton().setBackgroundDrawable(ToolBarItems.getDrawable("shop", WebActivityRef));
                ToolBarItems.getCartButton().setBackgroundDrawable(ToolBarItems.getDrawable("cart", WebActivityRef));
                ToolBarItems.getSignInButton().setBackgroundDrawable(ToolBarItems.getDrawable("accountgrey",WebActivityRef));
            } else {
                ToolBarItems.getContactButton().setBackground(ToolBarItems.getDrawable("contactblue", WebActivityRef));
                ToolBarItems.getShopButton().setBackground(ToolBarItems.getDrawable("shop", WebActivityRef));
                ToolBarItems.getCartButton().setBackground(ToolBarItems.getDrawable("cart", WebActivityRef));
                ToolBarItems.getSignInButton().setBackground(ToolBarItems.getDrawable("accountgrey", WebActivityRef));
            }
        } else if (url.equals(WebActivity.URL_LOGIN_STATIC)) {
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                ToolBarItems.getContactButton().setBackgroundDrawable(ToolBarItems.getDrawable("contact", WebActivityRef));
                ToolBarItems.getShopButton().setBackgroundDrawable(ToolBarItems.getDrawable("shop", WebActivityRef));
                ToolBarItems.getCartButton().setBackgroundDrawable(ToolBarItems.getDrawable("cart", WebActivityRef));
                ToolBarItems.getSignInButton().setBackgroundDrawable(ToolBarItems.getDrawable("accountblue", WebActivityRef));
            } else {
                ToolBarItems.getContactButton().setBackground(ToolBarItems.getDrawable("contact", WebActivityRef));
                ToolBarItems.getShopButton().setBackground(ToolBarItems.getDrawable("shop", WebActivityRef));
                ToolBarItems.getCartButton().setBackground(ToolBarItems.getDrawable("cart", WebActivityRef));
                ToolBarItems.getSignInButton().setBackground(ToolBarItems.getDrawable("accountblue", WebActivityRef));
            }
        } else if (url.equals(WebActivity.URL_CART)) {
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                ToolBarItems.getContactButton().setBackgroundDrawable(ToolBarItems.getDrawable("contact", WebActivityRef));
                ToolBarItems.getShopButton().setBackgroundDrawable(ToolBarItems.getDrawable("shop", WebActivityRef));
                ToolBarItems.getCartButton().setBackgroundDrawable(ToolBarItems.getDrawable("cartblue", WebActivityRef));
                ToolBarItems.getSignInButton().setBackgroundDrawable(ToolBarItems.getDrawable("accountgrey", WebActivityRef));
            } else {
                ToolBarItems.getContactButton().setBackground(ToolBarItems.getDrawable("contact", WebActivityRef));
                ToolBarItems.getShopButton().setBackground(ToolBarItems.getDrawable("shop", WebActivityRef));
                ToolBarItems.getCartButton().setBackground(ToolBarItems.getDrawable("cartblue", WebActivityRef));
                ToolBarItems.getSignInButton().setBackground(ToolBarItems.getDrawable("accountgrey", WebActivityRef));
            }
        }
    }

    //not UI thread safe
    private void animate(final WebView view, int duration, Interpolator transitionEffect) {
        boolean startAnimation = true;

        if (startAnimation) {
            // getOriginalY = view.getY();
            Log.d("MyValue", Integer.toString((int) originalPosition));
            view.setY(Graphics.displaySize.y * -1);
            translation = new TranslateAnimation(0f, 0F, 0f, Graphics.displaySize.y);
            translation.setStartOffset(0);
            translation.setDuration(duration);
            translation.setFillEnabled(true);
            translation.setInterpolator(transitionEffect);
            translation.setAnimationListener(new Animation.AnimationListener() {
                                                 @Override
                                                 public void onAnimationStart(Animation animation) {
                                                     Log.d("MyValue", "Running");
                                                     Handler handler = new Handler(Looper.getMainLooper());
                                                     handler.post(new Runnable() {
                                                         /**
                                                          * Starts executing the active part of the class' code. This method is
                                                          * called when a thread is started that has been created with a class which
                                                          * implements {@code Runnable}.
                                                          */
                                                         @Override
                                                         public void run() {
                                                             view.setVisibility(View.VISIBLE);
                                                         }
                                                     });

                                                 }

                                                 @Override
                                                 public void onAnimationEnd(Animation arg0) {
                                                     //Functionality here
                                                     Log.d("MyValue", "Finished");
                                                     Handler handler = new Handler(Looper.getMainLooper());
                                                     handler.post(new Runnable() {
                                                         /**
                                                          * Starts executing the active part of the class' code. This method is
                                                          * called when a thread is started that has been created with a class which
                                                          * implements {@code Runnable}.
                                                          */
                                                         @Override
                                                         public void run() {
                                                             view.setY(originalPosition);
                                                             view.setVisibility(View.VISIBLE);
                                                             view.requestLayout();
                                                         }
                                                     });
                                                 }

                                                 @Override
                                                 public void onAnimationRepeat(Animation animation) {

                                                 }
                                             }

            );
                view.startAnimation(translation);
            }
        }

    private static class RefreshLoginStatusForUser extends AsyncTask<Void, Void, Void> {

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Void doInBackground(Void... params) {

            while (true) {
                Log.d("Service", "Refreshing login interval");
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */
                    @Override
                    public void run() {
                        resourceForNotLoggedInUser.loadUrl(WebActivity.URL_REFRESH_LOGIN_STATUS);
                    }
                });
                try {
                    Thread.sleep(WebActivity.REFRESH_INTERVAL_FOR_REFRESH_IN_MINUTES * 1000 * 60);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class WaitBeforeLookingAtJavascriptForNonLoggedInUser extends AsyncTask<Void, Void, Void> {

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Void doInBackground(Void... params) {

            try {
                Thread.sleep(TIME_TO_SLEEP_BEFORE_CHECKING_HTML);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final int[] progress = new int[1];
            progress[0] = -1;
            while (true) {

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    /**
                     * Starts executing the active part of the class' code. This method is
                     * called when a thread is started that has been created with a class which
                     * implements {@code Runnable}.
                     */
                    @Override
                    public void run() {
                        progress[0] = resourceForNotLoggedInUser.getProgress();
                    }
                });


                while (progress[0] < 100) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }

                if (progress[0] == 100) {
                    break;
                }
            }

            return null;
        }

        @Override
        protected synchronized void onPostExecute(Void result) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                             /**
                              * Starts executing the active part of the class' code. This method is
                              * called when a thread is started that has been created with a class which
                              * implements {@code Runnable}.
                              */
                             @Override
                             public void run() {
                                 resourceForNotLoggedInUser.loadUrl("javascript:window.HtmlViewer.showHTML"+
                                         "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                             }
                         });
            Log.d("HTML", "Release another update");
            WebActivity.lockForGettingCartDataForUserNotLoggedIn.release();
        }

    }

}